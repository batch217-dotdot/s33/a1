// #3 #4

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let resultArr = [];
	json.map((item) => {
		resultArr.push(item.title);
	});
	console.log(resultArr);
});

// #5 #6
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => {
	console.log(json);
	console.log(`The item "${json.title}" on the list has a status of ${json.completed}`);
});

// #7
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Create To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "Pending",
		status: "Pending",
		title: "delectus aut autem",
		userId: 1
	})
});

// #10 #11
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});